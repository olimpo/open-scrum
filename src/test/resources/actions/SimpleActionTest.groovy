package actions

import com.openscrum.business.service.Action
import com.openscrum.common.spring.AppContext
import com.openscrum.business.service.UserService
import com.openscrum.data.model.*;

class SimpleActionTest implements Action {

	public void execute() {
		// Get service instance using spring context.
		UserService service = AppContext.getApplicationContext().getBean("userService")

		assert service

		def admin = service.findByLogin("admin@apolo.br")
		println "System ADM " + admin == null ? "<null>" : admin.getName()
	}
}
