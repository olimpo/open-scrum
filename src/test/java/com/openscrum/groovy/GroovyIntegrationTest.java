package com.openscrum.groovy;

import groovy.lang.GroovyClassLoader;

import org.junit.Assert;
import org.junit.Test;

import com.openscrum.BaseSpringTest;
import com.openscrum.business.service.Action;

public class GroovyIntegrationTest extends BaseSpringTest {

	@Test
	public void testRun() throws Exception {
		try {
			GroovyClassLoader gcl = new GroovyClassLoader();
			Class<?> clazz = gcl.parseClass(getClass().getClassLoader().getResourceAsStream("actions/SimpleActionTest.groovy"));
			Object groovyScript = clazz.newInstance();

			Action action = (Action) groovyScript;
			action.execute();
			
			gcl.close();
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
