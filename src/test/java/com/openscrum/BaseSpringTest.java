package com.openscrum;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.openscrum.common.config.RootConfig;
import com.openscrum.common.config.SecurityConfig;
import com.openscrum.common.config.ThymeleafConfig;
import com.openscrum.common.config.WebMvcConfig;

/**
 * Base class to be extended by business unit tests. This class already contains all Spring configuration that are necessary.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RootConfig.class, SecurityConfig.class, ThymeleafConfig.class, WebMvcConfig.class })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
@WebAppConfiguration
public abstract class BaseSpringTest {

}
