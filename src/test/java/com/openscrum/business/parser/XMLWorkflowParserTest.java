package com.openscrum.business.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import groovy.lang.GroovyClassLoader;

import org.junit.Test;
import org.springframework.util.StringUtils;

import com.openscrum.business.service.Action;
import com.openscrum.data.model.State;
import com.openscrum.data.model.Transition;
import com.openscrum.data.model.Workflow;

public class XMLWorkflowParserTest {

	@Test
	public void testParse() throws Exception {
		Workflow workflow = XMLWorkflowParser.parse(getClass().getClassLoader().getResourceAsStream("simple.xml"));

		assertNotNull(workflow);
		assertNotNull(workflow.getStates());
		assertEquals(workflow.getStates().size(), 6);

		for (State state : workflow.getStates()) {
			System.out.println(state.getName());
			assertNotNull(state.getName());
			assertNotNull(state.getTransitions());
			for (Transition transition : state.getTransitions()) {
				System.out.println("\t" + transition.getName());
				assertNotNull(transition.getName());
				assertNotNull(transition.getTarget());

				String action = transition.getAction() != null ? new String(transition.getAction(), "UTF-8") : ""; 
				if (StringUtils.hasText(action)) {
					GroovyClassLoader gcl = new GroovyClassLoader();
					Class<?> clazz = gcl.parseClass(action, "action.groovy");
					Object groovyScript = clazz.newInstance();

					Action actionExec = (Action) groovyScript;
					actionExec.execute();
					
					gcl.close();
				}
			}
		}
	}

}
