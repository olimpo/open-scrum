package com.openscrum.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Workflow.class)
public abstract class Workflow_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SingularAttribute<Workflow, String> description;
	public static volatile SingularAttribute<Workflow, String> name;
	public static volatile SetAttribute<Workflow, State> states;

}

