package com.openscrum.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SetAttribute<User, Project> projects;
	public static volatile SingularAttribute<User, String> pictureGeneratedName;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SingularAttribute<User, String> name;
	public static volatile SingularAttribute<User, String> pictureOriginalName;
	public static volatile SingularAttribute<User, Project> currentProject;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SetAttribute<User, UserGroup> groups;

}

