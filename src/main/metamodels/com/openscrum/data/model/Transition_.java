package com.openscrum.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Transition.class)
public abstract class Transition_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SingularAttribute<Transition, String> name;
	public static volatile SingularAttribute<Transition, State> state;
	public static volatile SingularAttribute<Transition, byte[]> action;
	public static volatile SingularAttribute<Transition, String> target;

}

