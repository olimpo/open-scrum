package com.openscrum.data.model;

import com.openscrum.security.UserPermission;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserGroup.class)
public abstract class UserGroup_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SetAttribute<UserGroup, User> users;
	public static volatile SingularAttribute<UserGroup, String> name;
	public static volatile SetAttribute<UserGroup, UserPermission> permissions;

}

