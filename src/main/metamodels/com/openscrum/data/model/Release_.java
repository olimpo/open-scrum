package com.openscrum.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Release.class)
public abstract class Release_ extends com.openscrum.data.model.AuditableBaseEntity_ {

	public static volatile SingularAttribute<Release, String> description;
	public static volatile SingularAttribute<Release, String> name;

}

