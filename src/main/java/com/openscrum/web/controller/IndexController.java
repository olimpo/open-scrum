package com.openscrum.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.openscrum.common.util.MessageBundle;
import com.openscrum.web.enums.Navigation;
import com.openscrum.web.service.BreadCrumbTreeService;

@Controller
public class IndexController extends BaseController {

	@Autowired
	protected BreadCrumbTreeService breadCrumbService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {
		Authentication auth = getAuthentication();
		if (auth == null) {
			return Navigation.AUTH_LOGIN.getPath();
		}

		breadCrumbService.addNode(MessageBundle.getMessageBundle("breadcrumb.home"), 0, request);
		return Navigation.INDEX.getPath();
	}

}
