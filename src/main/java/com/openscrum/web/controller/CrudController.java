package com.openscrum.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.repository.BaseRepository.SortDirection;

public abstract class CrudController<E extends BaseEntity> extends BaseController {

	/**
	 * In implementation use annotation like this example:
	 * 
	 * @SecuredEnum(UserPermission.permission_name)
	 * @RequestMapping(value = "path", method = RequestMethod.GET)
	 * @return ModelAndView
	 */
	public abstract ModelAndView list(HttpServletRequest request);

	/**
	 * In implementation use annotation like this example:
	 * 
	 * @SecuredEnum({ UserPermission.permission_name })
	 * @RequestMapping(value = "path", method = RequestMethod.POST)
	 * @param entity
	 * @return ModelAndView
	 */
	public abstract ModelAndView save(E entity, BindingResult result, HttpServletRequest request);

	/**
	 * In implementation use annotation like this example:
	 * 
	 * @SecuredEnum(UserPermission.permission_name)
	 * @RequestMapping(value = "path", method = RequestMethod.GET)
	 * @return ModelAndView
	 */
	public abstract ModelAndView create(HttpServletRequest request);

	/**
	 * In implementation use annotation like this example:
	 * 
	 * @SecuredEnum(UserPermission.permission_name)
	 * @RequestMapping(value = "path/{id}", method = RequestMethod.GET)
	 * @param id
	 * @return ModelAndView
	 */
	public abstract ModelAndView edit(@PathVariable Long id, HttpServletRequest request);

	/**
	 * In implementation use annotation like this example:
	 * 
	 * @SecuredEnum(UserPermission.permission_name)
	 * @RequestMapping(value = "path/{id}", method = RequestMethod.GET)
	 * @param id
	 * @return String - Json with operation return message
	 */
	public abstract @ResponseBody String remove(@PathVariable Long id);

	protected SortDirection getSortDir(String dir) {
		if ("desc".equals(dir)) {
			return SortDirection.DESCENDING;
		}
		return SortDirection.ASCENDING;

	}

	protected String getActions(final Long id, final String rootPath, final String permissionsView, final String permissionsEdit,
			final String permissionsRemove, final HttpServletRequest request) {
		boolean allowed = false;

		String actionsColumnPerm = "" + permissionsView + permissionsEdit + permissionsRemove;
		if (StringUtils.hasText(actionsColumnPerm)) {
			allowed = hasAnyPermission(actionsColumnPerm, request);
		} else {
			allowed = true;
		}
		if (allowed) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("<div class=\"btn-group-xs\">");

			String rootContext = request.getContextPath();

			if (hasAnyPermission(permissionsView, request)) {
				buffer.append("<a href=\"");
				buffer.append(rootContext);
				buffer.append("/");
				buffer.append(rootPath);
				buffer.append("/view/");
				buffer.append(id);
				buffer.append("\" class=\"btn btn-sm\"><i class=\"glyphicon glyphicon-search\"></i>");
				buffer.append(MessageBundle.getMessageBundle("common.show"));
				buffer.append("</a>");
			}
			if (hasAnyPermission(permissionsEdit, request)) {
				buffer.append("<a href=\"");
				buffer.append(rootContext);
				buffer.append("/");
				buffer.append(rootPath);
				buffer.append("/edit/");
				buffer.append(id);
				buffer.append("\" class=\"btn btn-sm\"><i class=\"glyphicon glyphicon-edit\"></i>");
				buffer.append(MessageBundle.getMessageBundle("common.edit"));
				buffer.append("</a>");
			}
			if (hasAnyPermission(permissionsRemove, request)) {
				buffer.append("<a onclick=\"removeConfirmationDialogOpen('");
				buffer.append(rootContext);
				buffer.append("/");
				buffer.append(rootPath);
				buffer.append("/remove/");
				buffer.append(id);
				buffer.append("');\" class=\"btn btn-sm\"><i class=\"glyphicon glyphicon-remove\"></i>");
				buffer.append(MessageBundle.getMessageBundle("common.remove"));
				buffer.append("</a>");
			}
			buffer.append("</div>");
			return buffer.toString();
		}
		return null;
	}

}
