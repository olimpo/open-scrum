package com.openscrum.data.entitylistener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.core.context.SecurityContextHolder;

import com.openscrum.data.model.User;
import com.openscrum.data.model.interfaces.IAuditableEntity;
import com.openscrum.security.CurrentUser;


public class AuditListener {

	@PrePersist
	void onCreate(IAuditableEntity e) {
		User user = new User();
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if (principal != null && !"anonymousUser".equals(principal)) {
			CurrentUser currentUser = (CurrentUser) principal;
			
			if (currentUser != null) {
				user.setId(currentUser.getId());
				
				e.setCreatedBy(user);
				e.setLastUpdatedBy(user);
			}
		} else {
			e.setCreatedBy(null);
			e.setLastUpdatedBy(null);
		}

		e.setCreationDate(new Date());
		e.setLastUpdateDate(new Date());
	}

	@PreUpdate
	void onPersist(IAuditableEntity e) {
		User user = new User();
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if (principal != null && !"anonymousUser".equals(principal)) {
			CurrentUser currentUser = (CurrentUser) principal;
			
			if (currentUser != null) {
				user.setId(currentUser.getId());
				
				e.setLastUpdatedBy(user);
			}
		} else {
			e.setLastUpdatedBy(null);
		}

		e.setLastUpdateDate(new Date());
	}
}
