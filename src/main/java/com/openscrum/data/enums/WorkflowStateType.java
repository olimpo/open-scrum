package com.openscrum.data.enums;

public enum WorkflowStateType {

	START, PATH, END

}
