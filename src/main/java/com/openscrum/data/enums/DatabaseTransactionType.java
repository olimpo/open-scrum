package com.openscrum.data.enums;

import com.openscrum.common.util.MessageBundle;

public enum DatabaseTransactionType {
	CREATE {
		@Override
		public String getName() {
			return MessageBundle.getMessageBundle("auditlog.databaseTransactionType.CREATE");
		}
	}, UPDATE {
		@Override
		public String getName() {
			return MessageBundle.getMessageBundle("auditlog.databaseTransactionType.UPDATE");
		}
	}, DELETE {
		@Override
		public String getName() {
			return MessageBundle.getMessageBundle("auditlog.databaseTransactionType.DELETE");
		}
	};
	
	/**
	 * Strategy to get the view name of this enum
	 * @return String
	 */
	public abstract String getName();
}
