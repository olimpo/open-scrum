package com.openscrum.data.repository;

import com.openscrum.data.model.UserStory;

public interface UserStoryRepository extends BaseRepository<UserStory, Long> {

}
