package com.openscrum.data.repository;

import com.openscrum.data.model.Project;


public interface ProjectRepository extends BaseRepository<Project, Long> {

	
}
