package com.openscrum.data.repository;

import com.openscrum.data.model.User;


public interface UserRepository extends BaseRepository<User, Long>{

	User findUserByEmail(String email);

}
