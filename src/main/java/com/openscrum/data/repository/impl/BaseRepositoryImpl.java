package com.openscrum.data.repository.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.log4j.Logger;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.openscrum.common.exception.NoDataFoundException;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.repository.BaseRepository;


/**
 * Basic implementation of the {@link BaseRepository} class.
 * 
 * @param <E>
 *            Generic Entity (inherits from {@link BaseEntity}) associated with this Data Access Object Class.
 */
@NoRepositoryBean
public  class BaseRepositoryImpl<E, ID extends Serializable> extends SimpleJpaRepository<E, ID> implements BaseRepository<E, ID> {
	protected static final Logger _logger = Logger.getLogger(BaseRepositoryImpl.class);

	
	private EntityManager entityManager;
	private Class<E> domainClass;

	public BaseRepositoryImpl(Class<E> domainClass, EntityManager em) {
		super(domainClass, em);

		// This is the recommended method for accessing inherited class dependencies.
		this.entityManager = em;
		this.domainClass = domainClass;
	}

	/**
	 * Execute basic search (no parging nor sorting) which filter the entered fields using the entered parameter.
	 * 
	 * @throws NoDataFoundException
	 * @throws DatabaseException
	 */
	@Override
	public List<E> search(String param, List<SingularAttribute<? extends BaseEntity, String>> fields) throws NoDataFoundException {
		try {
			CriteriaQuery<E> query = createBasicCriteriaForSearch(param, fields, domainClass, null, null);

			return entityManager.createQuery(query).getResultList();
		} catch (NoResultException e) {
			throw new NoDataFoundException(e);
		} catch (PersistenceException e) {
			_logger.error("OperationalRepositoryImpl.search() -- Error filtering resutls.", e);
			throw new PersistenceException(e);
		}
	}

	/**
	 * Perform a basic search using pagination and sorting. It filters using the <i>param</i> parameter into the <i>filters</i> parameter filters.
	 * 
	 * @throws NoDataFoundException
	 * @throws DatabaseException
	 */
	@Override
	public List<E> search(String param, List<SingularAttribute<? extends BaseEntity, String>> fields, Integer pageSize, Integer pageIndex,
			String sortField, SortDirection sortDirection) throws NoDataFoundException {
		try {
			// add basic criteria
			CriteriaQuery<E> query = createBasicCriteriaForSearch(param, fields, domainClass, sortField, sortDirection);

			TypedQuery<E> result = entityManager.createQuery(query);

			// add paging in case there are elements
			if (pageSize != null) {
				result.setMaxResults(pageSize);
			}
			if (pageIndex != null) {
				result.setFirstResult(pageIndex);
			}

			return result.getResultList();

		} catch (NoResultException e) {
			throw new NoDataFoundException(e);
		} catch (PersistenceException e) {
			_logger.error("OperationalRepositoryImpl.search() -- Error filtering resutls.", e);
			throw new PersistenceException(e);
		}

	}

	@Override
	public int count(String param, List<SingularAttribute<? extends BaseEntity, String>> fields) {

		try {
			// create criteria for count
			CriteriaQuery<Long> query = createBasicCriteriaForCount(param, fields, domainClass, null, null);
			// do count
			return entityManager.createQuery(query).getSingleResult().intValue();
		} catch (NoResultException e) {
			return 0;
		} catch (PersistenceException e) {
			_logger.error("OperationalRepositoryImpl.count() -- Error filtering resutls.", e);
			return 0;
		}
	}

	/**
	 * Create the criteria for searching, retrieving the count select.
	 * 
	 * @param param
	 *            Parameter to be filtered in the search.
	 * @param fields
	 *            Fields which will be filtered by the entered parameter.
	 * @param classType
	 *            Entity Class Type.
	 * @param sortField
	 *            Sort field.
	 * @param sortDirection
	 *            Sort Direction.
	 * 
	 * @return Criteria holding count select query for a basic search.
	 */
	protected CriteriaQuery<Long> createBasicCriteriaForCount(String param, List<SingularAttribute<? extends BaseEntity, String>> fields,
			Class<E> classType, String sortField, SortDirection sortDirection) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> query = cb.createQuery(Long.class);
		Root<E> root = query.from(classType);
		CriteriaQuery<Long> select = query.select(cb.count(root));

		concatCriteria(param, fields, sortField, sortDirection, cb, select, root);

		return query;
	}

	/**
	 * Create a basic search query.
	 * 
	 * @param param
	 *            Parameter to be filtered in the search.
	 * @param fields
	 *            Fields which will be filtered by the entered parameter.
	 * @param classType
	 *            Entity Class Type.
	 * @param sortField
	 *            Sort field.
	 * @param sortDirection
	 *            Sort Direction.
	 * 
	 * @return The basic search query.
	 */
	protected CriteriaQuery<E> createBasicCriteriaForSearch(String param, List<SingularAttribute<? extends BaseEntity, String>> fields,
			Class<E> classType, String sortField, SortDirection sortDirection) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> query = criteriaBuilder.createQuery(classType);
		Root<E> root = query.from(classType);

		concatCriteria(param, fields, sortField, sortDirection, criteriaBuilder, query, root);

		return query;
	}

	/**
	 * Add to the query the filtering elements.
	 * 
	 * @param param
	 *            Parameter which will be used as the filter for the query.
	 * @param fields
	 *            Fields which will be filtered.
	 * @param sortField
	 *            Sort field.
	 * @param sortDirection
	 *            Sort direction.
	 * @param criteriaBuilder
	 *            Criteria Builder.
	 * @param query
	 *            query which the filtered will be concatenated on.
	 * @param root
	 *            Root element of the query.
	 */
	@SuppressWarnings("unchecked")
	protected <T> void concatCriteria(String param, List<SingularAttribute<? extends BaseEntity, String>> fields, String sortField,
			SortDirection sortDirection, CriteriaBuilder criteriaBuilder, CriteriaQuery<T> query, Root<E> root) {

		if (StringUtils.hasText(param)) {
			param = "%" + param.toUpperCase() + "%";

			if (fields != null && !fields.isEmpty()) {
				Predicate[] criteriaLikes = new Predicate[fields.size()];

				for (int i = 0; i < fields.size(); i++) {
					criteriaLikes[i] = criteriaBuilder
							.like(criteriaBuilder.upper(root.get((SingularAttribute<E, String>) fields.get(i))), param);
				}

				query.where(criteriaBuilder.or(criteriaLikes));
			}
		}

		concatOrderBy(sortField, sortDirection, criteriaBuilder, query, root);
	}

	/**
	 * Add to the query the order by elements.
	 * 
	 * @param sortField
	 *            The field to sort by.
	 * @param sortDirection
	 *            ASC or DESC direction.
	 * @param criteriaBuilder
	 *            The current {@link CriteriaBuilder} instance.
	 * @param query
	 *            The current {@link CriteriaQuery} instance.
	 * @param from
	 *            The from path.
	 */
	protected <T> void concatOrderBy(String sortField, SortDirection sortDirection, CriteriaBuilder criteriaBuilder, CriteriaQuery<T> query,
			Path<E> from) {
		// add order by, in case there is any
		if (sortField != null) {
			Path<Object> sortPath = null;

			// handles complex sort field with lazy relationship (example:
			// usergroup.name).
			if (sortField.contains(".")) {
				String[] sortFieldPieces = sortField.split("\\.");
				if (sortFieldPieces.length > 0) {
					sortPath = from.get(sortFieldPieces[0]);
					for (int i = 1; i < sortFieldPieces.length; i++) {
						sortPath = sortPath.get(sortFieldPieces[i]);
					}
				}
			} else {
				sortPath = from.get(sortField);
			}

			if (sortDirection == null) {
				sortDirection = SortDirection.ASCENDING;
			}
			switch (sortDirection) {
				case ASCENDING:
					query.orderBy(criteriaBuilder.asc(sortPath));
					break;
				case DESCENDING:
					query.orderBy(criteriaBuilder.desc(sortPath));
					break;
			}

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <B> List<B> executeQuery(Class<B> clazz, String query, Object... params) {
		// Check query parameters.
		int queryParams = org.apache.commons.lang.StringUtils.countMatches(query, "?");
		if (queryParams > 0) {
			Assert.isTrue(params != null && queryParams == params.length,
					"The number of parameters on query is different of the lenght of params passed to method.");
		}

		Query jpaQuery = entityManager.createNativeQuery(query);
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				jpaQuery.setParameter((1 + i), params[i]);
			}
		}

		_logger.info("OperationalRepositoryImpl.executeQuery() -- WARN => Executing native query. " + query);

		// Holds the result before close the entity manager.
		List<B> result = jpaQuery.getResultList();

		// Close the entity manager to avoid keep multiple database connections.
		entityManager.close();

		return result;
	}

	@Override
	public <B> B attach(B entity) {
		Assert.notNull(entity);
		return entityManager.merge(entity);
	}
}
