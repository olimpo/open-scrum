package com.openscrum.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import com.openscrum.data.model.AuditLog;


public interface AuditLogRepository extends BaseRepository<AuditLog, Long> {
	
	Page<AuditLog> findByEntityNameLikeOrderByEntityNameAscOperationDateDesc(@Param("entityName") String entityName, Pageable page);

}
