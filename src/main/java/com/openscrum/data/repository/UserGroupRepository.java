package com.openscrum.data.repository;

import com.openscrum.data.model.UserGroup;


public interface UserGroupRepository extends BaseRepository<UserGroup, Long> {

}
