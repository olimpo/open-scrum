package com.openscrum.data.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_project")
@AttributeOverride(name = "id", column = @Column(name = "project_id"))
public class Project extends AuditableBaseEntity {
	private static final long serialVersionUID = -5449002490120052255L;

	@Column(name = "name", length = InputLength.NAME, nullable = false, unique = true)
	@NotNull
	@Size(min = 3, max = InputLength.NAME)
	private String name;

	@Column(name = "description", length = InputLength.DESCR)
	@Size(max = InputLength.DESCR)
	private String description;

	@Column(name = "dt_start")
	private Date startDate;

	@Column(name = "dt_end")
	private Date endDate;

	@Column(name = "dt_extended")
	private Date extendedDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.project", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<UserProjects> users;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getExtendedDate() {
		return extendedDate;
	}

	public void setExtendedDate(Date extendedDate) {
		this.extendedDate = extendedDate;
	}

	public Set<UserProjects> getUsers() {
		return users;
	}

	public void setUsers(Set<UserProjects> users) {
		this.users = users;
	}

}
