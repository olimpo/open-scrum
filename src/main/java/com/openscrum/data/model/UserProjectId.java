package com.openscrum.data.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Embeddable
public class UserProjectId implements Serializable {
	private static final long serialVersionUID = 5372020059768661391L;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private User user;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Project project;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserProjectId that = (UserProjectId) o;

		if (user != null ? !user.equals(that.user) : that.user != null)
			return false;
		if (project != null ? !project.equals(that.project) : that.project != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result;
		result = (user != null ? user.hashCode() : 0);
		result = 31 * result + (project != null ? project.hashCode() : 0);
		return result;
	}
}
