package com.openscrum.data.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;

/**
 * This class wrap the states and the logic of life cycle defined by XML.
 */
@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_workflow")
@AttributeOverride(name = "id", column = @Column(name = "workflow_id"))
public class Workflow extends AuditableBaseEntity {
	private static final long serialVersionUID = -80385563118028133L;

	@Column(name = "name", nullable = false, unique = true, length = InputLength.NAME)
	@NotNull
	@Size(min = 3, max = InputLength.NAME)
	private String name;

	@Size(max = InputLength.DESCR)
	@Column(name = "description", length = InputLength.DESCR)
	private String description;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "workflow")
	@OrderBy("state_id")
	private Set<State> states = new LinkedHashSet<State>();

	@Transient
	
	@NotNull
	private MultipartFile file;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<State> getStates() {
		return states;
	}

	public void setStates(Set<State> states) {
		this.states = states;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

}
