package com.openscrum.data.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.openscrum.data.entitylistener.AuditListener;
import com.openscrum.data.model.interfaces.IAuditableEntity;

@MappedSuperclass
@EntityListeners(value = AuditListener.class)
public abstract class AuditableBaseEntity extends BaseEntity implements IAuditableEntity {
	private static final long serialVersionUID = 7982765512946321298L;

	@JsonIgnore
	@Column(name = "creation_dt")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	private User createdBy;

	@JsonIgnore
	@Column(name = "last_update_dt")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "last_updated_by")
	private User lastUpdatedBy;

	@Override
	public Date getCreationDate() {
		return creationDate;
	}

	@Override
	public void setCreationDate(Date newCreationDate) {
		this.creationDate = newCreationDate;
	}

	@Override
	public User getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(User newCreatedBy) {
		this.createdBy = newCreatedBy;
	}

	@Override
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	@Override
	public void setLastUpdateDate(Date newLastUpdateDate) {
		this.lastUpdateDate = newLastUpdateDate;
	}

	@Override
	public User getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	@Override
	public void setLastUpdatedBy(User newLastUpdatedBy) {
		this.lastUpdatedBy = newLastUpdatedBy;
	}

}
