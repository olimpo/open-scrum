package com.openscrum.data.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_release")
@AttributeOverride(name = "id", column = @Column(name = "release_id"))
public class Release extends AuditableBaseEntity {
	private static final long serialVersionUID = -4030273024607391694L;

	@Column(name = "name", length = InputLength.NAME, nullable = false, unique = true)
	private String name;

	@Column(name = "description", length = InputLength.DESCR)
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
