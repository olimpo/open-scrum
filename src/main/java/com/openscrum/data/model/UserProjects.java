package com.openscrum.data.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.openscrum.data.enums.UserProjectRole;

@Entity
@Table(name = "t_users_in_project")
@AssociationOverrides({ @AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "user_id")),
		@AssociationOverride(name = "pk.project", joinColumns = @JoinColumn(name = "project_id")) })
public class UserProjects extends BaseEntity {
	private static final long serialVersionUID = 4080783211519840L;

	@EmbeddedId
	private UserProjectId pk = new UserProjectId();

	@Column(name = "project_role", nullable = false)
	@Enumerated(EnumType.STRING)
	private UserProjectRole role;

	public UserProjectId getPk() {
		return pk;
	}

	public void setPk(UserProjectId pk) {
		this.pk = pk;
	}

	public UserProjectRole getRole() {
		return role;
	}

	public void setRole(UserProjectRole role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserProjects that = (UserProjects) o;

		if (getPk() != null ? !getPk().equals(that.getPk()) : that.getPk() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (getPk() != null ? getPk().hashCode() : 0);
	}

}
