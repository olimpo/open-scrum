package com.openscrum.data.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;

/**
 * This class represents a transition into life cycle.
 */
@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_transition")
@AttributeOverride(name = "id", column = @Column(name = "transition_id"))
public class Transition extends AuditableBaseEntity {
	private static final long serialVersionUID = -9083100906827248992L;

	@Column(name = "name", nullable = false, length = InputLength.NAME)
	private String name;

	@Column(name = "target", nullable = false, length = InputLength.NAME)
	private String target;

	@JsonIgnore
	@Column(name = "action")
	@Lob
	private byte[] action;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "state_id", nullable = false)
	private State state;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public byte[] getAction() {
		return action;
	}

	public void setAction(byte[] action) {
		this.action = action;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Transient
	public String getStateName() {
		return state != null ? state.getName() : "";
	}

}
