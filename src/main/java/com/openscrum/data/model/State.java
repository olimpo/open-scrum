package com.openscrum.data.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;
import com.openscrum.data.enums.WorkflowStateType;

/**
 * This class represents a State into life cycle.
 */
@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_state")
@AttributeOverride(name = "id", column = @Column(name = "state_id"))
public class State extends AuditableBaseEntity {
	private static final long serialVersionUID = 9218678519681829500L;

	@Column(name = "name", nullable = false, length = InputLength.NAME)
	private String name;

	@Column(name = "state_type", nullable = false, length = InputLength.SHORT_VALUE)
	@Enumerated(EnumType.STRING)
	private WorkflowStateType type;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workflow_id", nullable = false)
	private Workflow workflow;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "state")
	@OrderBy("transition_id")
	private Set<Transition> transitions = new LinkedHashSet<Transition>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WorkflowStateType getType() {
		return type;
	}

	public void setType(WorkflowStateType type) {
		this.type = type;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public Set<Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Set<Transition> transitions) {
		this.transitions = transitions;
	}

	@Transient
	public Boolean getIsFinal() {
		return type == WorkflowStateType.END;
	}

	@Transient
	public Boolean getIsStart() {
		return type == WorkflowStateType.START;
	}

}