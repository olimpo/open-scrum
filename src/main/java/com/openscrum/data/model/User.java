package com.openscrum.data.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.openscrum.common.util.InputLength;
import com.openscrum.data.entitylistener.AuditLogListener;
import com.openscrum.security.UserPermission;

@Entity
@EntityListeners(value = AuditLogListener.class)
@Table(name = "t_user")
@AttributeOverride(name = "id", column = @Column(name = "user_id"))
public class User extends AuditableBaseEntity {
	private static final long serialVersionUID = 5588722501578237833L;

	@Column(name = "name", length = InputLength.MEDIUM, nullable = false)
	@NotNull
	@Size(min = 3, max = InputLength.MEDIUM)
	private String name;

	@Column(name = "email", unique = true, length = InputLength.SMALL, nullable = false)
	@NotNull
	@Size(min = 5, max = InputLength.SMALL)
	private String email;

	@Column(name = "password", length = InputLength.MEDIUM, nullable = false)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "t_users_in_groups", joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "group_id", referencedColumnName = "user_group_id") })
	@NotNull
	private Set<UserGroup> groups;

	@Transient
	private Set<UserPermission> permissions;

	@Column(name = "pic_original_name", length = InputLength.MEDIUM, nullable = true)
	private String pictureOriginalName;

	@Column(name = "pic_generated_name", length = InputLength.MEDIUM, nullable = true)
	private String pictureGeneratedName;

	@Transient
	private List<MultipartFile> picturefiles;

	
//	@ManyToMany(mappedBy = "users", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
//	private Set<Project> projects;

	// @OneToOne
	// @JoinColumn(name = "project_id", updatable = true)
	// private Project currentProject;

	public String getName() {
		return name;
	}

	public void setName(String value) {
		name = value;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password1) {
		this.password = password1;
	}

	public Set<UserGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<UserGroup> groups1) {
		this.groups = groups1;
	}

	public Set<UserPermission> getPermissions() {
		if ((permissions == null || permissions.isEmpty()) && groups != null) {
			permissions = new HashSet<UserPermission>();
			for (UserGroup ug : groups) {
				if (ug.getPermissions() != null && !ug.getPermissions().isEmpty()) {
					permissions.addAll(ug.getPermissions());
				}
			}
		}
		return permissions;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPictureOriginalName() {
		return pictureOriginalName;
	}

	public void setPictureOriginalName(String pictureOriginalName) {
		this.pictureOriginalName = pictureOriginalName;
	}

	public String getPictureGeneratedName() {
		return pictureGeneratedName;
	}

	public void setPictureGeneratedName(String pictureGeneratedName) {
		this.pictureGeneratedName = pictureGeneratedName;
	}

	public List<MultipartFile> getPicturefiles() {
		return picturefiles;
	}

	public void setPicturefiles(List<MultipartFile> picturefiles) {
		this.picturefiles = picturefiles;
	}

	// public Set<Project> getProjects() {
	// return projects;
	// }
	//
	// public void setProjects(Set<Project> projects) {
	// this.projects = projects;
	// }

	// public Project getCurrentProject() {
	// return currentProject;
	// }
	//
	// public void setCurrentProject(Project currentProject) {
	// this.currentProject = currentProject;
	// }

}
