package com.openscrum.common.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.filter.Filter;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.springframework.util.StringUtils;

import com.openscrum.common.exception.GenericException;

public final class JDOMUtils {

	private static final Logger _logger = Logger.getLogger(JDOMUtils.class);

	public static Document getDocument(String xml) {
		try {
			SAXBuilder builder = new SAXBuilder();
			Document jdomDoc = builder.build(new ByteArrayInputStream(xml.getBytes()));
			return jdomDoc;
		} catch (JDOMException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	public static Document getDocument(File f) {
		try {
			SAXBuilder builder = new SAXBuilder();
			InputStream in = new FileInputStream(f);
			Document jdomDoc = builder.build(in);
			return jdomDoc;
		} catch (JDOMException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	public static Document getDocument(InputStream in) {
		try {
			SAXBuilder builder = new SAXBuilder();
			Document jdomDoc = builder.build(in);
			return jdomDoc;
		} catch (JDOMException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	public static Document getDocument(URL url) {
		try {
			SAXBuilder builder = new SAXBuilder();
			Document jdomDoc = builder.build(url);
			return jdomDoc;
		} catch (JDOMException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	public Document getDocument(String configFilename, boolean readFromClasspath) {
		InputStream is = null;
		if (readFromClasspath) {
			is = this.getClass().getClassLoader().getResourceAsStream(configFilename);
		} else {
			try {
				is = new FileInputStream(new File(configFilename));
			} catch (FileNotFoundException e) {
				_logger.error("JDOMUtils.getDocument() -- Error opening file input stream.", e);
			}
		}
		// Reads XML file
		SAXBuilder builder = new SAXBuilder();
		Document xmlDoc = null;
		try {
			xmlDoc = builder.build(is);
		} catch (JDOMException e) {
			_logger.error("JDOMUtils.getDocument() -- Error building sax.", e);
		} catch (IOException e) {
			_logger.error("JDOMUtils.getDocument() -- Error with io.", e);
		}
		// Close the inputStream.
		try {
			is.close();
		} catch (IOException e) {
			_logger.error("JDOMUtils.getDocument() -- Error closing file input stream.", e);
		}
		return xmlDoc;
	}

	public static Filter newTagFilter(final String name) {
		Filter f = new Filter() {
			private static final long serialVersionUID = 1L;

			public boolean matches(Object arg0) {
				if ((arg0 instanceof Element) && ((Element) arg0).getName().equals(name)) {
					return true;
				} else {
					return false;
				}
			}
		};

		return f;
	}

	public static List<?> find(Object start, String xpath) {
		try {
			List<?> nodes = XPath.selectNodes(start, xpath);
			if (nodes == null) {
				nodes = new ArrayList<Object>();
			}
			return nodes;
		} catch (JDOMException e) {
			_logger.error("Erro", e);
			throw new GenericException(e);
		}
	}

	public static List<Element> findElements(Object start, String xpath) {
		List<?> objs = find(start, xpath);
		List<Element> result = new ArrayList<Element>();
		for (Object o : objs) {
			if (o instanceof Element) {
				result.add((Element) o);
			}
		}
		return result;
	}

	public static Element findSingleElement(Object start, String xpath) {
		List<Element> l = findElements(start, xpath);
		if (l.size() > 0) {
			return l.get(0);
		} else {
			return null;
		}
	}

	public static Document stripNamespaces(Document jdomDoc) {
		stripNamespaces(jdomDoc.getRootElement());
		return jdomDoc;
	}

	@SuppressWarnings("unchecked")
	private static void stripNamespaces(Element element) {
		element.setNamespace(null);

		for (Attribute a : (List<Attribute>) element.getAttributes()) {
			a.setNamespace(null);
		}

		for (Element child : (List<Element>) element.getChildren()) {
			stripNamespaces(child);
		}
	}

	public static String toStringFormatted(Document jdomDoc) {
		XMLOutputter out = new XMLOutputter();
		out.setFormat(Format.getPrettyFormat());
		return out.outputString(jdomDoc);
	}

	public static String toStringRaw(Document jdomDoc) {
		XMLOutputter out = new XMLOutputter();
		out.setFormat(Format.getRawFormat());
		return out.outputString(jdomDoc);
	}

	@SuppressWarnings("unchecked")
	public static Element descend(Element root, String path) {
		Element current = root;
		String[] nodes = StringUtils.delimitedListToStringArray(path, "/");
		for (String p : nodes) {
			if (!StringUtils.hasText(p))
				continue;

			String name = p;
			int index = 0;

			int i = p.indexOf("[");
			if (i > -1) {
				name = p.substring(0, i);
				index = Integer.valueOf(p.substring(i + 1, p.indexOf("]")));
			}

			List<Element> children = current.getChildren();

			if (StringUtils.hasText(name))
				children = CollectionUtils.find(children, "name", name);

			if (children.size() <= index)
				return null;

			current = children.get(index);
		}
		return current;
	}
}
