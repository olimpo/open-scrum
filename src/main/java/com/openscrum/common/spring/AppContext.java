package com.openscrum.common.spring;

import org.springframework.context.ApplicationContext;

/**
 * This class provides an application-wide access to the Spring ApplicationContext! The ApplicationContext is injected in a static method of the class
 * "AppContext".
 * 
 * Use AppContext.getApplicationContext() to get access to all Spring Beans.
 * 
 * @author Siegfried Bolz
 */
public class AppContext {

	private static ApplicationContext ctx;

	/**
	 * Injected from the class "ApplicationContextProvider" which is automatically loaded during Spring-Initialization.
	 */
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ctx = applicationContext;
	}

	/**
	 * Get access to the Spring ApplicationContext from everywhere in your Application.
	 * 
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		return ctx;
	}

}
