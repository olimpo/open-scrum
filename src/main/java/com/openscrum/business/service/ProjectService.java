package com.openscrum.business.service;

import com.openscrum.data.model.Project;

public interface ProjectService extends BaseService<Project, Long> {

}
