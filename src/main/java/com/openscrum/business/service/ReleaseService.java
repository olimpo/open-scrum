package com.openscrum.business.service;

import com.openscrum.data.model.Release;

public interface ReleaseService extends BaseService<Release, Long> {

}
