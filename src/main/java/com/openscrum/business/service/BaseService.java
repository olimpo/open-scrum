package com.openscrum.business.service;

import java.io.Serializable;
import java.util.List;

import com.openscrum.business.model.SearchResult;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.exception.NoDataFoundException;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.repository.BaseRepository.SortDirection;

/**
 * The base interface for all services.
 * 
 * @param <E>
 *            The entity associated with this service, which must inherit from {@link Serializable}.
 */
public interface BaseService<E extends BaseEntity, ID extends Serializable> extends Serializable {

	/**
	 * List all entities.
	 * 
	 * @return All entities.
	 */
	List<E> list();

	/**
	 * Find an entity by its unique identifier.
	 * 
	 * @param id
	 *            Unique identifier.
	 * 
	 * @return Entity related to a unique identifier.
	 */
	E find(ID id);

	/**
	 * Save an entity.
	 * 
	 * @param entity
	 *            Entity to be saved.
	 * 
	 * @return The just-saved-entity with its unique identifier updated, it case it happens.
	 * @throws AccessDeniedException
	 */
	E save(E entity) throws AccessDeniedException;

	/**
	 * Delete an entity.
	 * 
	 * @param entity
	 *            Entity to be deleted.
	 * @throws AccessDeniedException
	 */
	void remove(E entity) throws AccessDeniedException;

	/**
	 * Search for entities using the entered parameters as the criteria to do so. No paging nor sorting is performed.
	 * 
	 * @param param
	 *            Parameter used as search criteria.
	 * 
	 * @return List of entities filtered by the entered parameter.
	 * @throws DatabaseException
	 * @throws NoDataFoundException
	 */
	SearchResult<E> search(String param) throws NoDataFoundException;

	/**
	 * Search for entities using the entered parameters as the criteria to do so. Paging and sorting is performed.
	 * 
	 * @param param
	 *            Parameter used as search criteria.
	 * @param pageSize
	 *            Number of records to be retrieved.
	 * @param pageIndex
	 *            Index of the first record to be retrieved.
	 * @param sortField
	 *            Sort field name.
	 * @param sortDirection
	 *            Sort Direction.
	 * 
	 * @return List of entities filtered by the entered parameter.
	 * @throws DatabaseException
	 * @throws NoDataFoundException
	 */
	SearchResult<E> search(String param, Integer pageSize, Integer pageIndex, String sortField, SortDirection sortDirection) throws NoDataFoundException;

	/**
	 * Count the number of records which would be retrieved by a search using <i>param</i> parameter as a query filter.
	 * 
	 * @param param
	 *            Parameter used as search criteria.
	 * 
	 * @return Number of records which would be retrieved by a search using <i>param</i> parameter as a query filter.
	 */
	int count(String param);

	/**
	 * Count the number of records.
	 * 
	 * @return Number of records.
	 */
	int count();

}
