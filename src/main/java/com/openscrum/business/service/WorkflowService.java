package com.openscrum.business.service;

import com.openscrum.data.model.Workflow;

public interface WorkflowService extends BaseService<Workflow, Long> {

}
