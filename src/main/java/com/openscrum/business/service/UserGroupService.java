package com.openscrum.business.service;

import java.util.List;

import com.openscrum.data.model.User;
import com.openscrum.data.model.UserGroup;
import com.openscrum.security.UserPermission;


public interface UserGroupService extends BaseService<UserGroup, Long> {

	List<UserPermission> getUserPermissionList(final User loggedUser);

}