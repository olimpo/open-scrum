package com.openscrum.business.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.openscrum.business.model.FileContent;
import com.openscrum.data.model.User;

public interface UserService extends UserDetailsService, BaseService<User, Long> {

	User findByLogin(String login);

	User save(User user, boolean changePassword, FileContent file);

}