package com.openscrum.business.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openscrum.business.exception.WorkflowParserException;
import com.openscrum.business.parser.XMLWorkflowParser;
import com.openscrum.business.service.WorkflowService;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.exception.GenericException;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.model.Workflow;
import com.openscrum.data.model.Workflow_;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.WorkflowRepository;

@Service("workflowService")
public class WorkflowServiceImpl extends BaseServiceImpl<Workflow, Long> implements WorkflowService {
	private static final long serialVersionUID = -64188397124850992L;

	@Autowired
	private WorkflowRepository workflowRepository;

	@Override
	public List<Workflow> list() {
		return workflowRepository.findAll(new Sort(Direction.ASC, Workflow_.name.getName()));
	}

	@Override
	public Workflow find(Long id) {
		return workflowRepository.findOne(id);
	}

	@Transactional
	@Override
	public Workflow save(Workflow entity) throws AccessDeniedException {
		try {
			if (entity != null) {
				// Parse the XML to save workflow with states and transitions.
				entity = XMLWorkflowParser.parse(entity);
			}
			return workflowRepository.save(entity);
		} catch (WorkflowParserException | IOException | PersistenceException e) {
			throw new GenericException(e);
		}
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(Workflow_.name);
		fields.add(Workflow_.description);
		return fields;
	}

	@Override
	protected BaseRepository<Workflow, Long> getRepository() {
		return workflowRepository;
	}



}
