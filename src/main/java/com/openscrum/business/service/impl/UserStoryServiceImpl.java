package com.openscrum.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.openscrum.business.service.UserStoryService;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.model.UserStory;
import com.openscrum.data.model.UserStory_;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.UserStoryRepository;

@Service("userStoryService")
public class UserStoryServiceImpl extends BaseServiceImpl<UserStory, Long> implements UserStoryService {
	private static final long serialVersionUID = 4615696360898588274L;

	@Autowired
	private UserStoryRepository userStoryRepository;

	@Override
	public List<UserStory> list() {
		return (List<UserStory>) userStoryRepository.findAll(new Sort(Sort.Direction.ASC, UserStory_.priority.getName()));
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(UserStory_.name);
		fields.add(UserStory_.description);
		return fields;
	}

	@Override
	protected BaseRepository<UserStory, Long> getRepository() {
		return userStoryRepository;
	}

}
