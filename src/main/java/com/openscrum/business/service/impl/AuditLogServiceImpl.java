package com.openscrum.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.openscrum.business.model.SearchResult;
import com.openscrum.business.service.AuditLogService;
import com.openscrum.business.service.UserService;
import com.openscrum.common.exception.NoDataFoundException;
import com.openscrum.data.model.AuditLog;
import com.openscrum.data.model.AuditLog_;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.repository.AuditLogRepository;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.BaseRepository.SortDirection;

@Service(value = "auditLogService")
public class AuditLogServiceImpl extends BaseServiceImpl<AuditLog, Long> implements AuditLogService {
	private static final long serialVersionUID = 1027238581782745233L;

	@Autowired
	private AuditLogRepository auditLogRepository;
	@Autowired
	private UserService userService;

	@Transactional(propagation = Propagation.REQUIRED)
	public AuditLog save(AuditLog entity) {
		return auditLogRepository.saveAndFlush(entity);
	}

	@Override
	public List<AuditLog> list() {
		return (List<AuditLog>) auditLogRepository.findAll(new Sort(Sort.Direction.DESC, AuditLog_.operationDate.getName()));
	}

	@Override
	public void remove(AuditLog entity) {
		throw new NotImplementedException();
	}

	private void findAllOperationExecutors(SearchResult<AuditLog> result) {
		if (result.getResults() != null && !result.getResults().isEmpty()) {
			for (AuditLog auditLog : result.getResults()) {
				auditLog.setExecutedBy(userService.find(auditLog.getExecutedById()));
			}
		}
	}

	@Override
	public SearchResult<AuditLog> search(String param) throws NoDataFoundException {
		SearchResult<AuditLog> result = super.search(param);
		findAllOperationExecutors(result);
		return result;
	}

	@Override
	public SearchResult<AuditLog> search(String param, Integer pageSize, Integer pageIndex, String sortField, SortDirection sortDirection)
			throws NoDataFoundException {
		SearchResult<AuditLog> result = super.search(param, pageSize, pageIndex, sortField, sortDirection);
		findAllOperationExecutors(result);
		return result;
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(AuditLog_.entityName);
		return fields;
	}

	@Override
	protected BaseRepository<AuditLog, Long> getRepository() {
		return auditLogRepository;
	}
}
