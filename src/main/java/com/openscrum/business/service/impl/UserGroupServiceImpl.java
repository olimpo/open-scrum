package com.openscrum.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openscrum.business.service.UserGroupService;
import com.openscrum.common.exception.AccessDeniedException;
import com.openscrum.common.util.MessageBundle;
import com.openscrum.data.model.BaseEntity;
import com.openscrum.data.model.User;
import com.openscrum.data.model.UserGroup;
import com.openscrum.data.model.UserGroup_;
import com.openscrum.data.repository.BaseRepository;
import com.openscrum.data.repository.UserGroupRepository;
import com.openscrum.security.UserPermission;

@Service("userGroupService")
public class UserGroupServiceImpl extends BaseServiceImpl<UserGroup, Long> implements UserGroupService {
	private static final long serialVersionUID = 7584394584743075328L;

	@Autowired
	private UserGroupRepository userGroupRepository;

	@Override
	public List<UserGroup> list() {
		return (List<UserGroup>) userGroupRepository.findAll(new Sort(Sort.Direction.ASC, UserGroup_.name.getName()));
	}

	@Override
	@Transactional
	public UserGroup save(UserGroup userGroup) throws AccessDeniedException {
		if (userGroup != null && userGroup.getId() != null && userGroup.getId().equals(1L)) {
			throw new AccessDeniedException(MessageBundle.getMessageBundle("user.group.msg.access.denied"));
		} else {
			userGroup.setLastUpdateDate(new Date());
			return userGroupRepository.save(userGroup);
		}
	}

	@Override
	@Transactional
	public void remove(UserGroup userGroup) throws AccessDeniedException {
		if (userGroup != null && userGroup.getId().equals(1L)) {
			throw new AccessDeniedException(MessageBundle.getMessageBundle("user.group.msg.access.denied"));
		} else {
			userGroupRepository.delete(userGroup);
		}
	}

	@Override
	public List<UserPermission> getUserPermissionList(final User loggedUser) {
		List<UserPermission> permissions = new ArrayList<UserPermission>();

		for (UserPermission permission : UserPermission.values()) {
			if (UserPermission.ADMIN.equals(permission) && loggedUser.getPermissions().contains(UserPermission.ADMIN)) {
				permissions.add(permission);
			} else if (!UserPermission.ADMIN.equals(permission)) {
				permissions.add(permission);
			}
		}

		return permissions;
	}

	@Override
	protected List<SingularAttribute<? extends BaseEntity, String>> buildSearchFields() {
		List<SingularAttribute<? extends BaseEntity, String>> fields = new ArrayList<SingularAttribute<? extends BaseEntity, String>>();
		fields.add(UserGroup_.name);
		return fields;
	}

	@Override
	protected BaseRepository<UserGroup, Long> getRepository() {
		return userGroupRepository;
	}
}
