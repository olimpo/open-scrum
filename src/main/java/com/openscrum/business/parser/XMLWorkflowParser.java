package com.openscrum.business.parser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.openscrum.business.exception.AttributeNotFoundException;
import com.openscrum.business.exception.DuplicatedStateException;
import com.openscrum.business.exception.DuplicatedTransitionException;
import com.openscrum.business.exception.EndStateNotFoundException;
import com.openscrum.business.exception.StartStateNotFoundException;
import com.openscrum.business.exception.WorkflowParserException;
import com.openscrum.common.util.JDOMUtils;
import com.openscrum.data.enums.WorkflowStateType;
import com.openscrum.data.model.State;
import com.openscrum.data.model.Transition;
import com.openscrum.data.model.Workflow;

/**
 * This class implements an engine between xml edited by user and the program to manipulate the state machine.
 */
public class XMLWorkflowParser {

	private static final String ELEMENT_TRANSITION = "transition";
	private static final String ELEMENT_END_STATE = "end-state";
	private static final String ELEMENT_START_STATE = "start-state";
	private static final String ELEMENT_STATE = "state";

	private static final String ATTRIBUTE_NAME = "name";
	private static final String ATTRIBUTE_ACTION = "action";
	private static final String ATTRIBUTE_TO = "to";

	public static Workflow parse(File fileXml) throws WorkflowParserException {
		Document document = JDOMUtils.getDocument(fileXml);
		return constructNodes(document, new Workflow());
	}

	public static Workflow parse(InputStream stream) throws WorkflowParserException {
		Document document = JDOMUtils.getDocument(stream);
		return constructNodes(document, new Workflow());
	}

	public static Workflow parse(Workflow workflow) throws WorkflowParserException, IOException {
		Assert.notNull(workflow, "Workflow must not be null!");
		Assert.notNull(workflow.getFile(), "Workflow file must not be null!");

		Document document = JDOMUtils.getDocument(workflow.getFile().getInputStream());
		return constructNodes(document, workflow);
	}

	private static Workflow constructNodes(Document document, Workflow workflow) {
		Element root = document.getRootElement();
		if (!root.getName().equalsIgnoreCase("workflow")) {
			throw new WorkflowParserException("The root element must be \"workflow\".");
		}

		// The user can determine the name of workflow through interface.
		if (!StringUtils.hasText(workflow.getName())) {
			workflow.setName(root.getAttributeValue(ATTRIBUTE_NAME));
		}
		customStateElement(workflow, root, ELEMENT_START_STATE, WorkflowStateType.START, StartStateNotFoundException.class);
		customStateElement(workflow, root, ELEMENT_END_STATE, WorkflowStateType.END, EndStateNotFoundException.class);

		List<Element> elStates = JDOMUtils.findElements(root, ELEMENT_STATE);
		for (Element elState : elStates) {
			State state = new State();
			state.setName(getAttribute(elState, ATTRIBUTE_NAME));
			state.setWorkflow(workflow);
			state.setType(WorkflowStateType.PATH);

			setTransitions(elState, state);

			// Avoid duplicated states.
			for (State st : workflow.getStates()) {
				if (st.getName().equals(state.getName())) {
					throw new DuplicatedStateException();
				}
			}

			workflow.getStates().add(state);
		}

		return workflow;
	}

	/**
	 * Used to check <code>&lt;start-state&gt;</code> and <code>&lt;end-state&gt;</code> elements.
	 * 
	 * @param workflow
	 *            The current workflow being parsed.
	 * @param root
	 *            The XML root element.
	 * @param element
	 *            The element name ("start-state" or "end-state").
	 */
	private static void customStateElement(Workflow workflow, Element root, String elementName, WorkflowStateType type,
			Class<? extends WorkflowParserException> exceptionClass) {
		List<Element> elements = JDOMUtils.findElements(root, elementName);
		if (elements == null || elements.isEmpty()) {
			try {
				throw exceptionClass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new WorkflowParserException(e);
			}
		}
		Element elState = elements.get(0);
		State state = new State();
		state.setName(getAttribute(elState, ATTRIBUTE_NAME));
		state.setType(type);
		state.setWorkflow(workflow);
		setTransitions(elState, state);

		workflow.getStates().add(state);
	}

	@SuppressWarnings("unchecked")
	private static void setTransitions(final Element elState, final State state) {
		if (elState != null) {
			List<Element> elTrans = elState.getChildren(ELEMENT_TRANSITION);
			if (elTrans != null) {
				for (Element elTran : elTrans) {
					Transition transition = new Transition();
					transition.setName(getAttribute(elTran, ATTRIBUTE_NAME));
					transition.setTarget(getAttribute(elTran, ATTRIBUTE_TO));

					final String action = elTran.getChildText(ATTRIBUTE_ACTION);
					if (StringUtils.hasText(action)) {
						try {
							transition.setAction(action.getBytes("UTF-8"));
						} catch (UnsupportedEncodingException e) {
							// Do nothing.
						}
					}
					transition.setState(state);

					// Avoid duplicated transitions.
					for (Transition tran : state.getTransitions()) {
						if (tran.getName().equals(transition.getName()) || tran.getTarget().equals(transition.getTarget())) {
							throw new DuplicatedTransitionException();
						}
					}

					state.getTransitions().add(transition);
				}
			}
		}
	}

	private static String getAttribute(Element element, String attribute) throws AttributeNotFoundException {
		String value = element.getAttributeValue(attribute);
		if (element != null && !StringUtils.hasText(value)) {
			throw new AttributeNotFoundException("The attribute \"" + attribute + "\" is required for element \"" + element.getName() + "\".");
		}
		return value;
	}

}
