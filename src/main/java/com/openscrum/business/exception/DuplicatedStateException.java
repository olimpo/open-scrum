package com.openscrum.business.exception;

public class DuplicatedStateException extends WorkflowParserException {
	private static final long serialVersionUID = -1169872865680592595L;

	public DuplicatedStateException() {
		super();
	}

	public DuplicatedStateException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public DuplicatedStateException(String message) {
		super(message);
	}

	public DuplicatedStateException(Throwable throwable) {
		super(throwable);
	}

}
