package com.openscrum.business.exception;

public class StartStateNotFoundException extends WorkflowParserException {
	private static final long serialVersionUID = -5435688953133040040L;

	public StartStateNotFoundException() {
		super("The element \"start-state\" was expected.");
	}

	public StartStateNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public StartStateNotFoundException(String message) {
		super(message);
	}

	public StartStateNotFoundException(Throwable throwable) {
		super(throwable);
	}

}
