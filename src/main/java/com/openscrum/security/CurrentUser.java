package com.openscrum.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CurrentUser extends User {

	private static final long serialVersionUID = 8790909411747968450L;

	private Long id;
	
	private com.openscrum.data.model.User systemUser;

	public CurrentUser(Long id, String username, String password, com.openscrum.data.model.User systemUser, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.id = id;
		this.systemUser = systemUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public com.openscrum.data.model.User getSystemUser() {
		return systemUser;
	}

	public void setSystemUser(com.openscrum.data.model.User systemUser) {
		this.systemUser = systemUser;
	}

}
