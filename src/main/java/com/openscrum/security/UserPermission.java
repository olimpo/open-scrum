package com.openscrum.security;

import org.springframework.security.access.ConfigAttribute;

public enum UserPermission implements ConfigAttribute {

	// @formatter:off
	ADMIN,

	USER, 
	USER_CREATE, 
	USER_EDIT,
	USER_REMOVE,
	USER_LIST,

	USER_PERMISSION_LIST,
	USER_PERMISSION_VIEW,
	USER_PERMISSION_CREATE,
	USER_PERMISSION_EDIT,
	USER_PERMISSION_REMOVE,

	WORKFLOW_LIST,
	WORKFLOW_VIEW,
	WORKFLOW_CREATE,
	WORKFLOW_EDIT,
	WORKFLOW_REMOVE,

	PROJECT,
	PROJECT_CREATE,
	PROJECT_EDIT,
	PROJECT_REMOVE,
	PROJECT_LIST
	;
	
	// @formatter:on

	public String getAttribute() {
		return "ROLE_" + name(); // the spring security needs the ROLE prefix
	}
}
