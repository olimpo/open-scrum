INSERT INTO t_user (name, email, password) VALUES ('Administrator', 'admin@apolo.br', '1d0258c2440a8d19e716292b231e3190');

INSERT INTO t_user_group (name, created_by, creation_dt) VALUES ('Administrators', 1, CURRENT TIMESTAMP);
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Administrators'), 'ADMIN');

INSERT INTO t_users_in_groups (user_id, group_id) VALUES ((SELECT user_id FROM t_user WHERE email = 'admin@apolo.br'), (SELECT user_group_id FROM t_user_group WHERE name = 'Administrators'));


INSERT INTO t_user_group (name, created_by, creation_dt) VALUES ('Users - Gerenciamento', 1, CURRENT TIMESTAMP);
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Users - Gerenciamento'), 'USER'); 
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Users - Gerenciamento'), 'USER_CREATE'); 
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Users - Gerenciamento'), 'USER_EDIT');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Users - Gerenciamento'), 'USER_REMOVE');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Users - Gerenciamento'), 'USER_LIST');

INSERT INTO t_user_group (name, created_by, creation_dt) VALUES ('Users', 1, CURRENT TIMESTAMP);
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Users'), 'USER');

INSERT INTO t_user_group (name, created_by, creation_dt) VALUES ('Permissions - Gerenciamento', 1, CURRENT TIMESTAMP);
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions - Gerenciamento'), 'USER_PERMISSION_LIST');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions - Gerenciamento'), 'USER_PERMISSION_CREATE');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions - Gerenciamento'), 'USER_PERMISSION_EDIT');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions - Gerenciamento'), 'USER_PERMISSION_REMOVE');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions - Gerenciamento'), 'USER_PERMISSION_VIEW');

INSERT INTO t_user_group (name, created_by, creation_dt) VALUES ('Permissions', 1, CURRENT TIMESTAMP);
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions'), 'USER_PERMISSION_LIST');
INSERT INTO t_group_permission (group_id, permission_name) VALUES ((SELECT user_group_id FROM t_user_group WHERE name = 'Permissions'), 'USER_PERMISSION_VIEW');


INSERT INTO t_user (name, email, password) VALUES ('Power user', 'user@apolo.br', '1d0258c2440a8d19e716292b231e3190');
INSERT INTO t_users_in_groups (user_id, group_id) VALUES ((SELECT user_id FROM t_user WHERE email = 'user@apolo.br'), (SELECT user_group_id FROM t_user_group WHERE name = 'Users - Gerenciamento'));
INSERT INTO t_users_in_groups (user_id, group_id) VALUES ((SELECT user_id FROM t_user WHERE email = 'user@apolo.br'), (SELECT user_group_id FROM t_user_group WHERE name = 'Permissions - Gerenciamento'));

INSERT INTO t_user (name, email, password) VALUES ('User', 'restr@apolo.br', '1d0258c2440a8d19e716292b231e3190');
INSERT INTO t_users_in_groups (user_id, group_id) VALUES ((SELECT user_id FROM t_user WHERE email = 'restr@apolo.br'), (SELECT user_group_id FROM t_user_group WHERE name = 'Users'));
